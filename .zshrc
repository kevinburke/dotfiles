TWILIO_ZSHRC='twilio/.zshrc.twilio'
if [ -f $TWILIO_ZSHRC ]; then
    source $TWILIO_ZSHRC
fi

PATHDIRS=(
    $HOME/bin
    $HOME/twilio/bin

    # Virtualenv dependencies
    $HOME/code/pytwilio.fab/venv/bin
    $HOME/code/sshbox/venv/bin
    $HOME/code/twilio-jsonapi/venv/bin
    $HOME/code/httpie/venv/bin

    # Golang bin directories
    $HOME/code/go/bin

    $HOME/bin/autojump/bin
    $HOME/.cabal/bin
    $HOME/code/google_appengine
    $HOME/code/cloudbees-sdk-1.5.1
    /Applications/Postgres.app/Contents/MacOS/bin
    /usr/local/mysql/bin
    /opt/local/bin
    /opt/local/sbin
    /usr/local/sbin
    /usr/local/Cellar/python32/3.2.3/bin
    /usr/local/Cellar/ruby/1.9.2-p290/bin
    /usr/local/Cellar/ruby/2.0.0-p195/bin
    /usr/local/Cellar/ruby/2.0.0-p247/bin
    $HOME/.gem/ruby/1.8/bin
    /usr/local/share/npm/bin
    /usr/local/texlive/2012/bin/universal-darwin
    /usr/local/Cellar/go/1.0.3/bin
    /usr/local/Cellar/node/0.10.12/bin
)

HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.history

for dir in $PATHDIRS; do
    if [ -d $dir ]; then
        path=($path $dir)
    fi
done

# append to beginning to pick up correct python.
path=($HOME/.rvm/bin /usr/local/bin $path)

# XXX put everything in the go folder
export GOPATH=~/code/go:~/code/matasano:~/code/snapchat-friends/server:$GOPATH

# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

fignore=($FIGNORE .hi .pyc .egg-info .o .beam .dSYM .un~)


# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"
#
setopt list_ambiguous
setopt menu_complete
setopt dvorak

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

SCREENSET=""
if [[ "$TERM" == "screen" ]];
then
    SCREENSET="[screen]\n"
fi

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)

# virtualenv settings
export WORKON_HOME=~/.envs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv

wrapper_location="/usr/local/bin/virtualenvwrapper.sh"
if [ -f "$wrapper_location" ]; then
    source "$wrapper_location"
fi

git_completion="/etc/bash_completion.d/git"
if [ -f $git_completion ]; then
    source $git_completion
fi

# autojump
(( $+commands[brew] )) && { 
    [[ -f `brew --prefix`/etc/autojump.sh ]] && . `brew --prefix`/etc/autojump.sh
}

# rvm settings
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
autoload -U compinit && compinit


# zsh settings
plugins=(fab git lein)
source $ZSH/oh-my-zsh.sh
function prompt_char {
    git branch >/dev/null 2>/dev/null && echo '±' && return
    hg root >/dev/null 2>/dev/null && echo '☿' && return
}

function get_time {
    local seconds=${1}
    local minutes=0
    local hours=0
    while [ $seconds -gt 60 ]; do
        ((minutes = minutes + 1))
        ((seconds = seconds - 60))
    done
    while [ $minutes -gt 60 ]; do
        ((hours = hours + 1))
        ((minutes = minutes - 60))
    done
    echo "$hours $minutes $seconds"
}

function precmd {
    DIR=$(pwd|sed -e "s!$HOME!~!");
    if [ ${#DIR} -gt 60 ]; then 
        cur="${DIR:0:27}...${DIR:${#DIR}-30}";
    else
        cur=${DIR}
    fi;
    set -A _elapsed $_elapsed $(( SECONDS-_start ))
    result="`get_time $_elapsed[-1]`"
    hours=$(echo $result | cut -f1 -d" ")
    minutes=$(echo $result | cut -f2 -d" ")
    seconds_int=$(echo $result | cut -f3 -d" ")
    if [ "$hours" -eq "0" ]; then
        if [ "$minutes" -eq "0" ]; then
            _time="$seconds_int"s
        else
            seconds=`printf "%02d" "$seconds_int"`
            _time="$minutes":"$seconds"
        fi
    else
        seconds=`printf "%02d" "$seconds_int"`
        minutes=`printf "%02d" "$minutes"`
        _time="$hours:$minutes:$seconds"
    fi
}

# see http://stackoverflow.com/a/2704689/329700
preexec () {
   (( $#_elapsed > 1000 )) && set -A _elapsed $_elapsed[-1000,-1]
   typeset -ig _start=SECONDS
}

function print_color () { 
    echo -ne "%{\e[38;05;${1}m%}"; 
}

# color guide: http://misc.flogisoft.com/_media/bash/colors_format/256-colors.sh.png
RED=`print_color 160`
ORANGE=`print_color 172`
BLUE=`print_color 79`
BRIGHTBLUE=`print_color 33`
YELLOW=`print_color 226`
GREEN=`print_color 82`
PURPLE=`print_color 170`
PROMPT_COLOR=`print_color 30`
if [ "$HOME" = '/home/kevin' -o "$HOME" = '/home/kevinburke' ]; then
    alias ls='ls --color';
    PROMPT_COLOR=`print_color 196`
fi

DEFAULT="%{$fg[default]%}"

ZSH_THEME_GIT_PROMPT_PREFIX="$GREEN("
ZSH_THEME_GIT_PROMPT_SUFFIX=")"

function git_status {
    inf=$(git_prompt_info);
    if [[ -n "$inf" ]]; then
        GITST=$(git_prompt_info)" "
    else
        GITST=''
    fi
}

add-zsh-hook precmd git_status

if [ "$USERNAME" = "root" ]; then
    user_color=$RED;
else
    user_color=$ORANGE;
fi

PROMPT='
$BLUE${_time} $SCREENSET$user_color%n${DEFAULT} in $YELLOW${cur}
$PURPLE`prompt_char` $GREEN$GITST%(?.$BRIGHTBLUE.$RED)$ ${DEFAULT}'

curlfn=`which curl`
curl() {
    noglob $curlfn $@; echo
}
alias curl='noglob curl'
alias make='nocorrect make'
alias jsonapi='noglob jsonapi'
alias gclone='cd ~/code && git clone'
alias gitst='git status'

alias inotebook='pushd ~/.ipython_notebooks; workon ipython && ipython notebook; deactivate; popd'

alias ll='ls -lah'
alias l=ll
alias less='less -M'
alias vi=vim
alias svi='sudo vim'
alias mv='mv -i'
alias cp='cp -iv'
alias grep='grep -i --color=auto'
alias jx='javac \!$.java ; java \!$'
alias viewmemory="ps -u $USER -o rss,command | grep -v peruser | awk '{sum+=\$1} END {print sum/1024}'"
alias viewprocesses="ps -u $USER -o rss,etime,pid,command"

alias mostusedcomms="history | awk '{CMD[\$2]++;count++;}END { for (a in CMD)print CMD[a] \" \" CMD[a]/count*100 \"% \" a;}' | grep -v \"./\" | column -c3 -s \" \" -t | sort -nr | nl |  head -n20"

alias srpm='sshbox --rpm'
alias vp='pushd ~/code/chef/vagrants/cluster; vagrant provision; big done; popd'
alias vssh='pushd ~/code/chef/vagrants/cluster; vagrant ssh; popd'
alias vup='pushd ~/code/chef/vagrants/cluster; vagrant up; big done; popd'
alias youtube='youtube-dl -t --extract-audio -f 18'
alias soundcloud='youtube-dl -t'
alias wifi='big C-VFBGUWJW'

push_branch() {
    branch=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
    git push $1 $branch
}
pull_branch() {
    branch=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
    git pull $1 $branch
}
autoload push_branch
autoload pull_branch

alias gpo='git push origin'
alias gpom='git push origin master'
alias gpob='push_branch origin'

alias gfo='git fetch origin'
alias gfot='git fetch origin --tags'
alias gfut='git fetch upstream --tags'
alias gpot='git push origin --tags'
alias gput='git push upstream --tags'

alias gpu='git push upstream'
alias gpum='git push upstream master'
alias gpub='push_branch upstream'

alias glo='git pull origin'
alias glom='git pull origin master'
alias glob='pull_branch origin'
alias glum='git pull upstream master'
alias glub='pull_branch upstream'

alias gci='git commit -m'

alias c='cd'
alias sd='say -vzarvox "done"'

# these save so much time
alias ..="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
alias ..4="cd ../../../.."
alias ..5="cd ../../../../.."
alias ..6="cd ../../../../../.."
alias ..7="cd ../../../../../../.."

# linux
(( $+commands[gvim] )) && {
    alias vi=gvim;
    alias svi='sudo gvim'
}

# set up macvim, if it exists
(( $+commands[mvim] )) && { 
    alias vi=mvim; 
    alias svi='sudo mvim'
    export EDITOR=/Users/kevin/bin/mvim
}

# ask for conf if deleting more than three files; otherwise, delete w/o conf
# use rm -I if it exists
(( $+commands[grm] )) && {
    alias rm='grm -I';
}

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

#function zle-line-init zle-keymap-select {
    #RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
    #RPS2=$RPS1
    #zle reset-prompt
#}
#zle -N zle-line-init
#zle -N zle-keymap-select
