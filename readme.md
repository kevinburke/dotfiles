# My dotfiles

Hope they're useful. I keep a lot of private things in .git/info/excludes,
which is not tracked by default.

# Install

Please be careful as this will overwrite any files in your current home
directory.

    cd
    git clone https://bitbucket.org/kevinburke/dotfiles.git
    cd dotfiles
    cp -na . ..

